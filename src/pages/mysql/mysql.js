import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';

import * as Material from '@material-ui/core';
import * as ComponentCustom from 'components';


class Mysql extends Component {

  constructor(props) {
    super(props)

    this.state = {
      value: "",
      list: [],
      openSnackBar: false,
      message: ""
    }
  }


  componentWillMount() {
    this.getAlllItems();
  }




  render() {

    const { classes } = this.props;

    return (
      <Grid container className={classes.root}>
        <div className={[classes.container]} >
          <h1 style={{ textAlign: "center" }} >Page for Mysql</h1>

          {/* Se añade componente customizable */}
          <ComponentCustom.Todolist list={this.state.list} />

          <form className={classes.formContainer} noValidate autoComplete="off">
            <Material.TextField
              id="standard-name"
              label="What needs to be done?"
              className={classes.textField}
              value={this.state.value}
              onChange={(e) => this.handleChange(e)}
              margin="normal"
            />

            <Grid container justify="flex-end">
              <Material.Button onClick={() => this.addItemLits()} variant="contained" color="primary" className={classes.button}>
                Agregar
              </Material.Button>
            </Grid>

          </form>


          <ComponentCustom.SnackAlert
            open={this.state.openSnackBar}
            onClose={this.closeSnackbar.bind(this)}
            message={this.state.message}
          />


        </div>
      </Grid>
    );
  }

  handleChange(e) {
    this.setState({ value: e.target.value });
  }

  /** @desc añade item a la llista de todoList */
  addItemLits() {

    if (this.state.value.length > 0) {
      let url = "http://192.168.3.167:3000/getTodoList/addNewItem";



      let temp = JSON.stringify({
        body: this.state.value
      });

      console.log(temp);

      fetch(url, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: temp
      })
        .then(response => {
          return response.json()
        })
        .then(response => {


          console.log(response);
          if (!response.error) {
            this.state.list.push({body:this.state.value})
            this.setState({ value: "" })
          } else {
            this.setState({ message: response.message })
            this.openSnackbar();
          }


        })
        .catch(error => {
          console.log(error);
        });

    } else {
      this.setState({ message: "El campo no puede estar vacio, por favor ingrese un valor" })
      this.openSnackbar();
    }
  }



  /** @desc optiene todos los items de la lista almacenados */
  getAlllItems() {
    let url = "http://192.168.3.167:3000/getTodoList/getAll";

    fetch(url, {
      method: 'GET',
    })
      .then(response => {
        return response.json()
      })
      .then(data => {

        console.log(data);

        let temp = this.state.list;
        temp.push(...data)
        this.setState({ list: temp })

      })
      .catch(error => {
        console.log(error);
      });
  }


  openSnackbar = () => {
    this.setState({ openSnackBar: true, });
  };

  closeSnackbar = () => {
    this.setState({ openSnackBar: false });
  };




}

const styles = {
  root: {
    // flex: 1,
    justifyContent: "center",
    backgroundColor: "transparent",
  },
  container: {
    width: "70%",
    backgroundColor: "transparent",
  },
  textField: {
    width: "100%"
  },
  formContainer: {
    width: "100%",
  },
  button: {
    width: "20%",
    marginRight: 0,
    alignItems: 'center',
    position: "relative",
    right: 0
  }
}



Mysql.propTypes = {
  classes: PropTypes.object.isRequired,
};



export default withStyles(styles)(Mysql);