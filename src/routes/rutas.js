

import React from "react";

// -- https://reacttraining.com/react-router/web/example/auth-workflow
// -- https://es.redux.js.org/docs/avanzado/uso-con-react-router.html
// -- https://codeburst.io/getting-started-with-react-router-5c978f70df91
import { BrowserRouter as Router, Route, Link } from "react-router-dom";



import Mysql from "pages/mysql/mysql";
import Mongo from "pages/mongo/mongo";
import Spring from "pages/spring/spring";

function AppRouter() {
    return (
        <Router>
            <div>

                {/* For navigation  between interfaces */}
                <nav>
                    <ul>
                        <li>
                            <Link to="/">Mysql</Link>
                        </li>
                        <li>
                            <Link to="/mongo/">Mongo</Link>
                        </li>
                        <li>
                            <Link to="/spring/">Spring</Link>
                        </li>
                    </ul>
                </nav>

                <Route path="/" exact component={Mysql} />
                <Route path="/mongo/" component={Mongo} />
                <Route path="/spring/" component={Spring} />
            </div>
        </Router>
    );
}

export default AppRouter;
