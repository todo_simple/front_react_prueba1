import React, { Component } from 'react'
import PropTypes from 'prop-types';


import { withStyles } from '@material-ui/core/styles';
import * as Material from '@material-ui/core';
import * as Icons from '@material-ui/icons';


class SnackAlert extends Component {



    render() {
        const { classes } = this.props;

        return (
            <div>

                <Material.Snackbar
                    anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
                    autoHideDuration={6000} // --- para ocultarse solo
                    open={this.props.open}
                    onClose={this.props.onClose}
                    ContentProps={{
                        'aria-describedby': 'message-id',
                    }}
                    message={<span id="message-id">{this.props.message}</span>}
                    action={[
                        <Material.IconButton
                            key="close"
                            aria-label="Close"
                            color="inherit"
                            className={classes.close}
                            onClick={this.props.onClose}
                        >
                            <Icons.Close />
                        </Material.IconButton>,
                    ]}
                />


            </div>
        )
    }
}


const styles = {
    root: {
        // flex: 1,
        justifyContent: "center",
        backgroundColor: "transparent",
    },
}



SnackAlert.propTypes = {
    classes: PropTypes.object.isRequired,
};



export default withStyles(styles)(SnackAlert);